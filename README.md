# NFox

#### 介绍
非狐的Cad工具类库，提供了大量的函数来简化cad的开发。

诚邀各路朋友参与开发这个cad工具类库。您可以提交代码，可以写注释，可以提交建议，可以写用法示例，可以帮忙宣传。

#### 软件架构

NFox 工具类库主要分为两个命名空间，一个是跟cad的操作直接相关的命名空间 **NFox.Cad**，一个是通用性的工具命名空间 **NFox.Basal**。

- NFox.Cad
- NFox.Basal
	- NFox.Collections
	- NFox.ComponentModel
	- NFox.Images
	- NFox.Runtime.Com
	- NFox.Runtime.Com.Reflection
	- NFox.Runtime
	- NFox.Runtime.Dll

#### 安装教程

1. 直接在nuget上搜索NFox，安装即可开始使用内裤的所有功能。
2. 或者在项目主页下载dll文件，然后在项目里添加引用。

由于NFox内裤采用的是standard类型的类库，因此建议采用nuget形式进行安装和使用。

采用standard类型类库和nuget的方式的好处是：

- .net的发展的趋势就是干掉frameworks类型，只保留standard(core)类型。frameworks在4.8版本之后就不在升级了，以后都会统一到.net 5。因此现在采用standard类型是一个正确的选择。
- 经过测试，cad是支持standard类型的类库加载和运行的，因此建议发现不可修复的bug之前，针对cad的二次开发也采用standard类型的类库。
- 采用nuget管理引用是可以不用管本地是否有要引用的dll文件，同时nuget安装的类库还会自动升级，方便采用更新的类库。

#### 使用说明

1. 快速入门

   - 打开vs，新建一个standard类型的类库项目。

   - 右键项目文件，选择管理nuget程序包。

   - 在nuget程序里搜索**AutoCAD.NET**，根据你的cad的版本选择包的版本号，然后点击安装。这里要注明的是：

     - AutoCAD.NET包是有桌子公司提供的，但是最低的版本为autocad2015以上，因此要开发autocad2015以下的版本的程序，请安装个人发布的程序包。
     - 经过测试，对于cad2013版本以上，可以直接使用net4.0进行开发，cad2012一下可以使用net3.5开发。同时经不完全测试，4.0以上的版本在高版本编译后可以用在低版本上。
     - 本内裤采用net4.5开发，主要依据为桌子公司提供的AutoCAD.NET包最低为net4.5版本的。

   - 在nuget程序里搜索**NFox**，直接选择最新的版本，然后点击安装。可以只安装NFox，因为NFox依赖AutoCAD.NET，nuget会自动将依赖安装，但是安装将是最新的版本，所以如果要控制版本的话，可以在项目文件里进行版本号指定。

   - 添加引用

    ```c#
     using Autodesk.AutoCAD.ApplicationServices;
     using Autodesk.AutoCAD.EditorInput;
     using Autodesk.AutoCAD.Runtime;
     using Autodesk.AutoCAD.Geometry;
     using Autodesk.AutoCAD.DatabaseServices;
     using NFox.Cad;
    ```

   - 添加代码

    ```c#
     [CommandMethod("hello")]
     public void Hello()
     {
       using (DBTransaction tr = new DBTransaction())
       {
         Line line1 = new Line(new Point3d(0, 0, 0), new Point3d(1, 1, 0));
         Circle circle = new Circle(new Point3d(0, 0, 0), Vector3d.ZAxis, 10);
         var btr = tr.OpenCurrentSpace();
         tr.AddEntity(btr, line1, circle);
       }
     }
    ```

     这段代码就是在cad的当前空间内添加了一条直线和一个圆。

   - F6生成，然后打开cad，netload命令将刚刚生成的dll加载。
   - 运行hello命令，然后缩放一下视图，现在一条直线和一个圆已经显示在屏幕上了。

2. [事务管理器用法](/doc/DBTransaction.md)

3. [选择集过滤器用法](/doc/SelectionFilter.md)

4. [符号表用法](/doc/SymbolTable.md)

5. [WPF支持](/doc/WPF.md)

6. 天秀的自动加载与初始化

   为了将程序集的初始化和通过写注册表的方式实现自动加载统一设置，减少每次重复的工作量，内裤提供了`AutoRegAssem`抽象类来完成此功能，只要在需要初始化的类继承`AutoRegAssem`类，然后实现`Initialize()` 和`Terminate()`两个函数就可以了。特别强调的是，一个程序集里只能有一个类继承，不管是不是一个命名空间。

   ```c#
   public class Test : AutoRegAssem //继承
   {
   		public override void Initialize() //实现接口函数
   		{
   				throw new NotImplementedException();
   		}
   		public override void Terminate() //实现接口函数
   		{
   				throw new NotImplementedException();
   		}
   }
   ```

7. 天秀的打开模式提权

   由于cad的对象是有打开模式，是否可写等等，为了安全起见，在处理对象时，一般是用读模式打开，然后需要写数据的时候在提权为写模式，然后在降级到读模式，但是这个过程中，很容易漏掉某些步骤，然后cad崩溃。为了处理这些情况，内裤提供了提权类来保证读写模式的有序转换。

   ```c#
   using(line.UpgradeOpenAndRun()) //开启对象写模式提权事务
   {
   		//处理代码
   } //关闭事务自动处理读写模式
   ```

8. 未完待续。。。。


#### 参与贡献

1. Fork 本仓库 
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)